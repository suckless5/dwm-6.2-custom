#!/bin/sh

m=$(amixer sget -D pulse Master | awk -F"[][]" '/Left:/ { print $4 }')

#Unmute
if [ "$m" = "on" ]
then
	xsetroot -name "[ Vol: $(amixer sget -D pulse Master | awk -F"[][]" '/Left:/ { print $2 }') ] [$(date "+%l:%M %p  %a, %m/%d/%Y") ]"
#Mute
else
	xsetroot -name "[ Vol: Mute ] [$(date "+%l:%M %p  %a, %m/%d/%Y") ]"
fi
