#!/bin/sh
nm-applet &
pasystray &
redshift-gtk &
udiskie &
unclutter &
~/.fehbg &
ulauncher --hide-window &
/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/geoclue-2.0/demos/agent &
xrdb ~/.Xresources &
while true; do
	~/.dwm/dwmstatus.sh
	sleep .5
done
