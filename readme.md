# dwm-6.2-custom
Custom dwm patched and configured for **dwm 6.2**

## Screenshot
![](/screenshot/dwm.png)

## dwm desktop location
```
/usr/share/xsessions/dwm.desktop
```

## autostart location
```
~/.dwm/autostart.sh
```
## dwmstatus script
```
~/.dwm/dwmstatus.sh
```

## Remember to make autorun.sh executable
```
chmod +x ~/.dwm/autostart.sh
```

## Programs:
### Install these programs from your linux distribution package manager.
1. alacritty or terminal emulator of your choice  
2. dmenu 
3. nemo 
4. chromium/firefox 
5. redshift 
6. feh 
7. network-manager-applet 
8. udiskie 
9. polkit-gnome/policykit-1-gnome 
10. geoclue2 
11. pulseaudio 
12. unclutter 
13. discord 
14. vol-notify 
15. powermenu 
16. awk
17. amixer

## vol-notify
repo: https://gitlab.com/window-managers/vol-notify

## powermenu
repo: https://gitlab.com/window-managers/powermenu

## dwm patches
1. autostart: https://dwm.suckless.org/patches/autostart/
2. systray: https://dwm.suckless.org/patches/systray/ 
3. fullgaps: https://dwm.suckless.org/patches/fullgaps/
4. push up/down: https://dwm.suckless.org/patches/push/ 
5. attachbottom: https://dwm.suckless.org/patches/attachbottom/ 

## Walpaper
### minimal mystic mountains
https://www.reddit.com/r/wallpapers/comments/hq8d53/minimalistic_mystical_mountains_1920_x_1080/?utm_medium=android_app&utm_source=share
